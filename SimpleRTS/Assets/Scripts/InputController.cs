using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Camera main;
    public Unit selectedUnit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if click/tap on unit, select; on ground, move unit
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if (hit.collider.tag == "unit")
                {
                    selectedUnit = hit.collider.gameObject.GetComponent<Unit>();
                }
                else if (hit.collider.tag == "map" && selectedUnit != null)
                {
                    selectedUnit.Move(hit.point);
                }
            }
        }
    }
}
