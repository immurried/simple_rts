using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour
{   
    NavMeshAgent agent;

    public STATS stats;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = stats.MoveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Move(Vector3 destinaton)
    {
        return agent.SetDestination(destinaton);
    }
}

[System.Serializable]
public struct STATS{
    public int HP;
    public float Armour;
    public int Damage;
    public float AttackSpeed;
    public float MoveSpeed;
}